# TCP - HTTPS IOT gateway proxy for AMB-NET  

This gateway proxy server will provide simple proxy service between embedded devices that support simple APDU binary protocol for mutual communication. The service will listen on port 8911.
Once a client connects to server the connection is opened for APDU_Timeout seconds or until first TCP packet with valid APDU arrives. Connection closes when APDU_Timeout seconds pass before first or any other expected APDU is recieved or error is sent in APDU response. In one way communication mode, server just listens to APDU commands and responds to them in request/response fashion. In two way communication node, reponse status code can be used to tell other side to wait until request APDU is sent the other way i.e. request from server to client. Either way the first APDU command can only be sent by the client connecting to server.
Initial communication between TCP client and TCP server will be in clear text, later it should be encrypted using standard Signal compantible protocol. The communication between server and third party remote service is encrypted using HTPPS (TLS1.3) protocol with server certificate verification (accept all certficates can be used during development if need to).

 In order to forward so called AMB event from TCP to HTTPS some basic business logic needs to be implemented so simple APDU command parsing is needed. Note: Each APDU message should not exceed 100kb.

 The communication flow overview with steps numbered:
 
 | Client | Server |
 | :--------------------------------------: | :--------------------------------------------------------------------------------------------------------------: |
 | (1) Sends APDU Write - Hello  | (2) Responds with '90ff' (OK Wait status) or error ('...') |
 | (4) Responds OK Wait or error | (3) Sends APDU Write - Start Event(s) command |
 | (5) Sends APDU Write - Send Event Data | (6) Posts JSON into rest API and returns OK wait if API returned 200  or ERROR if API returned some 400/500 status |
 | (8) Responds OK / Error | (7) Sends APDU Write - Event Accepted with hash of received Event Data  |

 Note: Send Event and Event Accepted can be looped until Client has more events to send

### Detail Communication APDUs

APDU is simple binary protocol containing header, data as request and binary response is always determined by request expected length i.e. in Write APDU command the only expected response is a two byte status word (0x9000 for OK or 0x90FF for OK WAIT  or 0x.... for error codes)

##### APDU WRITE Record (Format)
This is format for WRITE APDU binary command 
(7 + Lc Bytes long) APDU contents
 
1. **1B** ```CLA```  0xFF 
1. **1B** ```INS```  0xD2 
1. **1B** ```P1```  Record identifier (0x00 - 0xFF) 
1. **1B** ```P2```  Sub record (0x00 - 0xFF) 
1. **0B** ```Lc``` Length of the subsequent data field in extended APDU format (3 Bytes) to allow up to 65535 bytes to be written by one WRITE command. First byte of Lc is always 0x00 ( using Extended APDU  ) - Big Endian format. 
1. **NB** ```Data```  Record to be written of Lc Bytes ( specified in Lc field )
1. **3B** ```Le```  Empty (Only 2 Bytes of SW word is expected in return)

| 1.    | 2.    | 3.    | 4.    | 5.    | 5.    | 5.    | 6. | 6. | 6. |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |:---: |:---: |:---: | 
| ```CLA```   | ```INS```   | ```P1```    | ```P2```    | ```Lc0```   | ```Lc1```   | ```Lc2```  | ```Data 0``` | ```Data ...``` | ```Data Lc-1``` |   

#### APDU WRITE - Hello (Client Info)
Sends client info when connection is created to identify itself using both Eth Address EIP-51 format a.k.a. device identity and Asset ID its attached to (this could be used in future to create stateful session). 
NOTE: This Hello command is extra step in TCP protocol when compared to BTLE protocol since (unlike Perun) device connecting  to peripheral (AMB L1) device can read its identity from advertising packet or from another service or even out -of-band i.e. NFC

| 1.    | 2.    | 3.    | 4.    | 5.    | 5.    | 5.    | 6. | 6. |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |:---: |:---: |
| ```0xFF```   | ```0xD2```   | ```0x20```    | ```0x00```    | ```0x00```   | ```0x00```   | ```0x48```  | ```40 bytes of identity``` | ```32 bytes of Asset ID``` | `````` |  

Response 0x90ff or 0x8001 for error i.e. wrong length of APDU data, etc.
Now its time for server to process the info and respond with APDU - Start events.

#### APDU Write - Start Event(s) ( Ready to accept event)
Servers chance to get ready (i.e. verify identity, check if API ready ) and request client to Start Event generation.
Before events can be accepted server can (optional for now) verify client identity etc.
Server can use remote API to verify both Identity(Account) and Asset ID ( this process is optional now) see API: https://jsapi.apiary.io/previews/ambrosus/reference/asset/assets/fetch-an-asset-by-id

When ready server sends his APDU message:

| 1.    | 2.    | 3.    | 4.    | 5.    | 5.    | 5.    | 
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ```0xFF``` | ```0xD2``` | ```0x01```    | ```0x00``` | ```0x00``` | ```0x00``` | ```0x00``` |

Client response 0x90ff or 0x8001 for error i.e. wrong length of APDU data. 

When Start Event is received the device has chance to prepare event for broadcasting i.e. L1 will close and sign buffer to be sent 

#### APDU Write - Send Event Data
Client sends JSON  string (AMB event). 
Size Max for event JSON string is 64kB (in reality < 20kB due to remote API limitation on event size)

| 1.    | 2.    | 3.    | 4.    | 5.    | 5.    | 5.    | 6. | 6. | 6. |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |:---: |:---: |:---: |
| ```0xFF``` | ```0xD2``` | ```0x01``` | ```0x02``` | ```0x00``` | ```0xNN``` | ```0xNN``` | ```JSON event 1st byte``` | ```...``` | ```JSON event last byte``` |  

There are two possible alternatives for content inside the APDU data section:
1) pure JSON all ASCII characters that can be received and sent directly to Hermes REST API
2) hybrid ASCII/Binary characters that need to be first converted to ASCII only JSON. (currently in use by master branch)

The hybrid data (see event_1000 examples in ./docs) can be recognized by 2 ASCII lenghts prepended to start of the JSON string in the following format:   PREFIX_LEN:SUFFIX_LEN{....JSON STRING....}
The 2 lengths specify how long are ASCII sections in the received data. The first length specifies number of pre-fix bytes in the JSON STRING while the other lenght specifies number of suffix ASCII bytes of the JSON string which means that the rest of the bytes in the middle of JSON STRING are in binary and need to be converted to hex format before sending further to REST API.
For example hybrid data section could look like this:       12:13{EventData:[...],EventID:2}       Specifying that the first 12 bytes of JSON are ASCII chars "{EventData:[" then binary bytes follow and additional 13 bytes of JSON ASCII are appended in this case the string "],EventID:2}" . The ... are representing three binary bytes that need to be converted (bin-to-hex) to create final JSON string which may look like this after conversion:
  {EventData:[DE00AD],EventID:2} 
 
Server response 0x90FF if remote API returned OK status in the 200 range or 0x8001 for error i.e. wrong length of APDU data 
Possibly repost JSON data with modified signature ending byte if Signature authentication error was thrown by the API.

The JSON needs to be forwarded using HTTPS to the following REST API via POST methond  where the JSON is the message body and content type header application/json.

The URL has two variable parameters and looks like this:    https://[HOST_with_PORT]/assets/0x[ASSET_ID]/events 

HOTS_with_PORT could be any valid host name with optional port i.e. localhost:5555
ASSET_ID is form the client info Hello APDU above.

An example (test URL): https://gateway-test.ambrosus.com/assets/0xe79c12558091f6c29ce10308da0d9acee32992b4f48357c0a00bc03cf41cf3aa/events/


#### APDU Write - Event Accepted
Server sends SHA-256 hash of the sucessfully  posted JSON data (optionally may wait for confirmation) to remote device as a final confirmation.
 
| 1.    | 2.    | 3.    | 4.    | 5.    | 5.    | 5.    | 6. | 
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |:---: |
| ```0xFF```   | ```0xD2```   | ```0x01```    | ```0x01```    | ```0x00```   | ```0x00```   | ```0x20```  | ``` 32 bytes of SHA``` |

Client response 0x9000 for OK  or 0x8... for error i.e. wrong length of APDU data 
Client can respond with 0x90ff for OK and wait in order to send another event for the same ASSET ID


# Notes on two way APDU communication 

Ussually APDU is one way request/response but to improve throughput we implement APDU in chat style messaging which is especially usefull for encrypted payloads since their length is ussually not known to requesting party.

## 2-way communication
To sustain two way communication, we perform messages ping-pong by responding with special Status Word (SW).

This can be done with responding to any APDU command with one of following SW:
1. OK === 90 00 - executed correctly, B waits for another messages or disconnect
2. OK-WAIT === 90 FF - executed correcty, B will send some message back, e.g. APDU command
3. ANY ERROR SW - incorrect execution or state, leds to disconnect

-------

### Example Alice and Bob
Let`s assume case Alice (A) want to speak with Bob (B).

![ ](./diagram_2wayAPDU.jpg  "2-way APDU communication")

1. A  sends any APDU command to B.
2. B processes the command and responds with OK-WAIT.
3. A received OK-WAIT and waits.
4. B sends APDU command to A.
5. A processes the command and responds with OK-WAIT.
6. B  received OK and waits.
7. A sends disconnect to B and disconnects.
8. B disconnects.


```
--------------------------------
    HELLO
--------------------------------

 >>> device  to server : Bytes count = 51

ff d2 20 00 00 00 2C 01 02 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f 10 12 13 14 ...

<<< server to device
90 ff

--------------------------------
    START EVENT
--------------------------------

 >>> server to device : Bytes count = 11

ff d2 01 00 00 00 04 00 00 00 00

<<< device to server
90 ff


--------------------------------
    WRITE EVENT
--------------------------------

 >>> device to server : Bytes count = 2558

ff d2 f1 00 00 09 fe
31 31 30 3a 35 32 31 7b 22 65 76 65 6e 74 49 64 22 3a 22 30 78 35 34 39 38 34 65 34 31 31 61 30 61 63 31 31 37 31 66 65 66 32 64 36 62 61 62 65 39 38 66 39 34 63 39 35 32 62 63 30 37 38 65 36 34 36 35 31 62 64 66 36 66 30 64 32 37 30 61 30 66 34 34 37 64 22 2c 22 63 6f 6e 74 65 6e 74 22 3a 7b 22 64 61 74 61 22 3a 5b 7b 22 6c 31 5f 72 61 77 22 3a 22 1a 36 a5 5b 16 2d 48 01 2e 00 07 00 00 07 00 2e 1b 36 a5 5b 16 2d 48 01 2f 00 07 00 00 07 00 2e 1c 36 a5 5b 16 2d 48 01 23 00 08 00 00 07 00 2e 1d 36 a5 5b 16 2d 38 01 22 00 08 00 00 07 00 2e 1e 36 a5 5b 16 2d 38 01 2e 00 07 00 00 07 00 2e 1f 36 a5 5b 16 2d 40 01 2f 00 07 00 00 07 00 2e 20 36 a5 5b 16 2d 40 01 1a 00 08 01 00 07 00 2f 21 36 a5 5b 16 2d 40 01 2f 00 07 00 00 07 00 2e 22 36 a5 5b 16 2d 40 01 2e 00 07 00 00 07 00 2e 23 36 a5 5b 16 2d 40 01 2e 00 07 00 00 07 00 2e 24 36 a5 5b 16 2d 40 01 23 00 08 00 00 07 00 2e 25 36 a5 5b 16 2d 40 01 2f 00 07 00 00 07 00 2e 26 36 a5 5b 16 2d 38 01 22 00 08 00 00 07 00 2e 27 36 a5 5b 16 2d 40 01 2f 00 07 00 00 07 00 2e 28 36 a5 5b 16 2d 40 01 22 00 08 00 00 07 00 2e 29 36 a5 5b 16 2d 40 01 2e 00 07 00 00 07 00 2e 2a 36 a5 5b 16 2d 40 01 2e 00 07 00 00 07 00 2e 2b 36 a5 5b 16 2d 40 01 2e 00 07 00 00 07 00 2e 2c 36 a5 5b 16 2d 40 01 07 00 08 00 00 08 00 22 2d 36 a5 5b 16 2d 38 01 2e 00 07 00 00 07 00 2e 2e 36 a5 5b 16 2d 40 01 2f 00 07 00 00 07 00 2e 2f 36 a5 5b 16 2d 38 01 2e 00 07 00 00 07 00 2e 30 36 a5 5b 16 2d 38 01 22 00 08 00 00 07 00 2e 31 36 a5 5b 16 2d 38 01 2e 00 07 00 00 07 00 2e 32 36 a5 5b 16 2d 40 01 2e 00 07 00 00 07 00 2e 33 36 a5 5b 16 2d 38 01 2e 00 07 00 00 07 00 2e 34 36 a5 5b 16 2d 38 01 2e 00 07 00 00 07 00 2e 35 36 a5 5b 16 2d 38 01 2e 00 07 00 00 07 00 2e 36 36 a5 5b 16 2d 58 01 2e 00 07 00 00 07 00 2e 37 36 a5 5b 16 2d d8 00 2f 00 07 00 00 07 00 2e 38 36 a5 5b 16 2d 00 01 1f 00 07 00 00 07 00 2e 39 36 a5 5b 16 2d f8 00 10 00 08 01 00 07 00 2e 3a 36 a5 5b 16 2d 00 01 1e 00 07 00 00 07 00 2e 3b 36 a5 5b 16 2d 28 01 2e 00 07 00 00 07 00 2e 3c 36 a5 5b 16 2d 20 01 23 00 08 00 00 07 00 2e 3d 36 a5 5b 16 2d 20 01 2f 00 07 00 00 07 00 2e 3e 36 a5 5b 16 2d 20 01 2e 00 07 00 00 07 00 2e 3f 36 a5 5b 16 2d 20 01 0f 00 07 00 00 07 00 2e 40 36 a5 5b 16 2d 20 01 2f 00 07 00 00 07 00 2e 41 36 a5 5b 16 2d 20 01 22 00 08 00 00 07 00 2e 42 36 a5 5b 16 2d 18 01 13 00 08 00 00 07 00 2e 43 36 a5 5b 16 2d 20 01 2e 00 07 00 00 07 00 2e 44 36 a5 5b 16 2d 20 01 1e 00 07 00 00 07 00 2e 45 36 a5 5b 16 2d 28 01 2f 00 07 00 00 07 00 2e 46 36 a5 5b 16 2d 20 01 2e 00 07 00 00 07 00 2e 47 36 a5 5b 16 2d 28 01 2e 00 07 00 00 07 00 2e 48 36 a5 5b 16 2d 28 01 2e 00 07 00 00 07 00 2e 49 36 a5 5b 16 2d 08 01 13 00 08 00 00 07 00 2e 4a 36 a5 5b 16 2d f8 00 13 00 08 00 00 07 00 2e 4b 36 a5 5b 16 2d f8 00 12 00 08 00 00 07 00 2e 4c 36 a5 5b 16 2d 08 01 23 00 08 00 00 07 00 2e 4d 36 a5 5b 16 2d f8 00 13 00 08 00 00 07 00 2e 4e 36 a5 5b 16 2d 08 01 2e 00 07 00 00 07 00 2e 4f 36 a5 5b 16 2d 00 01 23 00 08 00 00 07 00 2e 50 36 a5 5b 16 2d 00 01 22 00 08 00 00 07 00 2e 51 36 a5 5b 16 2d 18 01 1e 00 07 00 00 07 00 2e 52 36 a5 5b 16 2d 20 01 1f 00 07 00 00 07 00 2e 53 36 a5 5b 16 2d 18 01 12 00 08 01 00 07 00 2e 54 36 a5 5b 16 2d 00 01 23 00 08 00 00 07 00 2e 55 36 a5 5b 16 2d 08 01 22 00 08 00 00 07 00 2e 56 36 a5 5b 16 2d 08 01 12 00 08 00 00 07 00 2e 57 36 a5 5b 16 2d 00 01 2f 00 07 00 00 07 00 2e 58 36 a5 5b 16 2d 00 01 22 00 08 00 00 07 00 2e 59 36 a5 5b 16 2d 00 01 1e 00 07 00 00 07 00 2e 5a 36 a5 5b 16 2d 08 01 2e 00 07 00 00 07 00 2e 5b 36 a5 5b 16 2d 10 01 1e 00 07 00 00 07 00 2e 5c 36 a5 5b 16 2d 00 01 22 00 08 00 00 07 00 2e 5d 36 a5 5b 16 2d f8 00 2f 00 07 00 00 07 00 2e 5e 36 a5 5b 16 2d 00 01 22 00 08 00 00 07 00 2e 5f 36 a5 5b 16 2d f8 00 2e 00 07 00 00 07 00 2e 60 36 a5 5b 16 2d 00 01 2f 00 07 00 00 07 00 2e 61 36 a5 5b 16 2d f8 00 1e 00 07 00 00 07 00 2e 62 36 a5 5b 16 2d 08 01 2f 00 07 00 00 07 00 2e 63 36 a5 5b 16 2d 28 01 12 00 08 00 00 07 00 2e 64 36 a5 5b 16 2d 28 01 2f 00 07 00 00 07 00 2e 65 36 a5 5b 16 2d 10 01 22 00 08 00 00 07 00 2e 66 36 a5 5b 16 2d f0 00 23 00 08 00 00 07 00 2e 67 36 a5 5b 16 2d f8 00 2e 00 07 00 00 07 00 2e 68 36 a5 5b 16 2d f8 00 22 00 08 00 00 07 00 2e 69 36 a5 5b 16 2d f0 00 22 00 08 00 00 07 00 2e 6a 36 a5 5b 16 2d f0 00 18 00 08 01 00 08 00 22 6b 36 a5 5b 16 2d f0 00 2f 00 07 00 00 07 00 2e 6c 36 a5 5b 16 2d f8 00 2e 00 07 00 00 07 00 2e 6d 36 a5 5b 16 2d f8 00 2f 00 07 00 00 07 00 2e 6e 36 a5 5b 16 2d f8 00 22 00 08 00 00 07 00 2e 6f 36 a5 5b 16 2d f8 00 22 00 08 00 00 07 00 2e 70 36 a5 5b 16 2d f8 00 1e 00 07 00 00 07 00 2e 71 36 a5 5b 16 2d 00 01 2e 00 07 00 00 07 00 2e 72 36 a5 5b 16 2d 00 01 22 00 08 00 00 07 00 2e 73 36 a5 5b 16 2d 00 01 2f 00 07 00 00 07 00 2e 74 36 a5 5b 16 2d f8 00 22 00 08 00 00 07 00 2e 75 36 a5 5b 16 2d f8 00 2f 00 07 00 00 07 00 2e 76 36 a5 5b 16 2d f8 00 2e 00 07 00 00 07 00 2e 77 36 a5 5b 16 2d f8 00 22 00 08 00 00 07 00 2e 78 36 a5 5b 16 2d e0 00 2e 00 07 00 00 07 00 2e 79 36 a5 5b 16 2d e8 00 22 00 08 00 00 07 00 2e 7a 36 a5 5b 16 2d f0 00 2e 00 07 00 00 07 00 2e 7b 36 a5 5b 16 2d f0 00 22 00 08 00 00 07 00 2e 7c 36 a5 5b 16 2d f8 00 2e 00 07 00 00 07 00 2e 7d 36 a5 5b 16 2d f0 00 2e 00 07 00 00 07 00 2e 7e 36 a5 5b 16 2d f8 00 2e 00 07 00 00 07 00 2e 7f 36 a5 5b 16 2d f8 00 2e 00 07 00 00 07 00 2e 80 36 a5 5b 16 2d f8 00 2e 00 07 00 00 07 00 2e 81 36 a5 5b 16 2d f8 00 2e 00 07 00 00 07 00 2e 82 36 a5 5b 16 2d f8 00 2f 00 07 00 00 07 00 2e 83 36 a5 5b 16 2d 00 01 2e 00 07 00 00 07 00 2e 84 36 a5 5b 16 2d 00 01 22 00 08 00 00 07 00 2e 85 36 a5 5b 16 2d f8 00 2e 00 07 00 00 07 00 2e 86 36 a5 5b 16 2d f8 00 22 00 08 00 00 07 00 2e 87 36 a5 5b 16 2d 00 01 2e 00 07 00 00 07 00 2e 88 36 a5 5b 16 2d 00 01 2e 00 07 00 00 07 00 2e 89 36 a5 5b 16 2d 00 01 2e 00 07 00 00 07 00 2e 8a 36 a5 5b 16 2d 00 01 2e 00 07 00 00 07 00 2e 8b 36 a5 5b 16 2d 00 01 2f 00 07 00 00 07 00 2e 8c 36 a5 5b 16 2d 00 01 2e 00 07 00 00 07 00 2e 8d 36 a5 5b 16 2d 00 01 22 00 08 00 00 07 00 2e 8e 36 a5 5b 16 2d 00 01 2e 00 07 00 00 07 00 2e 8f 36 a5 5b 16 2d 00 01 2e 00 07 00 00 07 00 2e 90 36 a5 5b 16 2d f0 00 2e 00 07 00 00 07 00 2e 91 36 a5 5b 16 2d f0 00 2e 00 07 00 00 07 00 2e 22 2c 22 74 79 70 65 22 3a 22 63 72 61 79 6f 6e 69 63 2e 61 6d 62 2e 6c 31 2e 64 61 74 61 2e 76 31 5f 30 5f 30 22 7d 5d 2c 22 69 64 44 61 74 61 22 3a 7b 22 61 63 63 65 73 73 4c 65 76 65 6c 22 3a 30 2c 22 61 73 73 65 74 49 64 22 3a 22 30 78 65 37 39 63 31 32 35 35 38 30 39 31 66 36 63 32 39 63 65 31 30 33 30 38 64 61 30 64 39 61 63 65 65 33 32 39 39 32 62 34 66 34 38 33 35 37 63 30 61 30 30 62 63 30 33 63 66 34 31 63 66 33 61 61 22 2c 22 63 72 65 61 74 65 64 42 79 22 3a 22 30 78 32 41 64 34 41 30 35 34 38 33 33 31 34 44 36 33 64 62 46 36 30 32 37 65 31 31 46 44 66 37 32 34 61 35 37 31 37 31 61 35 22 2c 22 64 61 74 61 48 61 73 68 22 3a 22 30 78 37 32 38 63 39 38 39 35 30 66 31 35 32 35 31 37 65 37 35 39 66 38 38 65 36 66 37 34 36 31 34 66 61 39 66 31 39 61 34 32 30 31 61 37 39 65 38 65 65 64 36 35 36 39 61 36 38 38 65 62 63 66 62 32 22 2c 22 74 69 6d 65 73 74 61 6d 70 22 3a 31 35 33 37 35 35 34 31 31 34 7d 2c 22 73 69 67 6e 61 74 75 72 65 22 3a 22 30 78 62 61 30 61 36 66 64 33 32 64 33 39 63 32 62 36 37 34 31 35 61 39 62 38 61 64 35 35 36 36 62 37 66 33 36 66 32 31 35 30 63 62 36 39 66 32 33 61 32 34 66 38 30 65 61 63 38 34 66 61 33 65 37 39 30 39 65 62 34 38 32 66 32 39 35 62 64 65 30 32 65 30 62 32 38 62 38 33 36 63 63 34 31 63 36 36 61 37 35 36 35 37 33 36 63 39 64 33 31 38 38 32 30 33 32 35 63 38 64 33 30 33 64 64 36 38 39 63 31 62 22 7d 7d 30 78 32 33 62 34 65 66 32 32 65 66 36 38 37 34 38 32 37 37 64 32 62 61 65 30 61 35 65 39 30 65 35 33 38 61 32 65 33 65 32 65 37 33 35 65 30 39 39 32 33 35 63 65 31 35 65 31 38 31 31 34 30 38 32 61


Server POSTs data to API example with response: 
 asset url : https://gateway-test.ambrosus.com/assets/0xe79c12558091f6c29ce10308da0d9acee32992b4f48357c0a00bc03cf41cf3aa/events HTTP response : 201 : {"eventId":"0x54984e411a0ac1171fef2d6babe98f94c952bc078e64651bdf6f0d270a0f447d","content":{"data":[{"l1_raw":"1a36a55b162d48012e0007000007002e1b36a55b162d48012f0007000007002e1c36a55b162d4801230008000007002e1d36a55b162d3801220008000007002e1e36a55b162d38012e0007000007002e1f36a55b162d40012f0007000007002e2036a55b162d40011a0008010007002f2136a55b162d40012f0007000007002e2236a55b162d40012e0007000007002e2336a55b162d40012e0007000007002e2436a55b162d4001230008000007002e2536a55b162d40012f0007000007002e2636a55b162d3801220008000007002e2736a55b162d40012f0007000007002e2836a55b162d4001220008000007002e2936a55b162d40012e0007000007002e2a36a55b162d40012e0007000007002e2b36a55b162d40012e0007000007002e2c36a55b162d400107000800000800222d36a55b162d38012e0007000007002e2e36a55b162d40012f0007000007002e2f36a55b162d38012e0007000007002e3036a55b162d3801220008000007002e3136a55b162d38012e0007000007002e3236a55b162d40012e0007000007002e3336a55b162d38012e0007000007002e3436a55b162d38012e0007000007002e3536a55b162d38012e0007000007002e3636a55b162d58012e0007000007002e3736a55b162dd8002f0007000007002e3836a55b162d00011f0007000007002e3936a55b162df800100008010007002e3a36a55b162d00011e0007000007002e3b36a55b162d28012e0007000007002e3c36a55b162d2001230008000007002e3d36a55b162d20012f0007000007002e3e36a55b162d20012e0007000007002e3f36a55b162d20010f0007000007002e4036a55b162d20012f0007000007002e4136a55b162d2001220008000007002e4236a55b162d1801130008000007002e4336a55b162d20012e0007000007002e4436a55b162d20011e0007000007002e4536a55b162d28012f0007000007002e4636a55b162d20012e0007000007002e4736a55b162d28012e0007000007002e4836a55b162d28012e0007000007002e4936a55b162d0801130008000007002e4a36a55b162df800130008000007002e4b36a55b162df800120008000007002e4c36a55b162d0801230008000007002e4d36a55b162df800130008000007002e4e36a55b162d08012e0007000007002e4f36a55b162d0001230008000007002e5036a55b162d0001220008000007002e5136a55b162d18011e0007000007002e5236a55b162d20011f0007000007002e5336a55b162d1801120008010007002e5436a55b162d0001230008000007002e5536a55b162d0801220008000007002e5636a55b162d0801120008000007002e5736a55b162d00012f0007000007002e5836a55b162d0001220008000007002e5936a55b162d00011e0007000007002e5a36a55b162d08012e0007000007002e5b36a55b162d10011e0007000007002e5c36a55b162d0001220008000007002e5d36a55b162df8002f0007000007002e5e36a55b162d0001220008000007002e5f36a55b162df8002e0007000007002e6036a55b162d00012f0007000007002e6136a55b162df8001e0007000007002e6236a55b162d08012f0007000007002e6336a55b162d2801120008000007002e6436a55b162d28012f0007000007002e6536a55b162d1001220008000007002e6636a55b162df000230008000007002e6736a55b162df8002e0007000007002e6836a55b162df800220008000007002e6936a55b162df000220008000007002e6a36a55b162df00018000801000800226b36a55b162df0002f0007000007002e6c36a55b162df8002e0007000007002e6d36a55b162df8002f0007000007002e6e36a55b162df800220008000007002e6f36a55b162df800220008000007002e7036a55b162df8001e0007000007002e7136a55b162d00012e0007000007002e7236a55b162d0001220008000007002e7336a55b162d00012f0007000007002e7436a55b162df800220008000007002e7536a55b162df8002f0007000007002e7636a55b162df8002e0007000007002e7736a55b162df800220008000007002e7836a55b162de0002e0007000007002e7936a55b162de800220008000007002e7a36a55b162df0002e0007000007002e7b36a55b162df000220008000007002e7c36a55b162df8002e0007000007002e7d36a55b162df0002e0007000007002e7e36a55b162df8002e0007000007002e7f36a55b162df8002e0007000007002e8036a55b162df8002e0007000007002e8136a55b162df8002e0007000007002e8236a55b162df8002f0007000007002e8336a55b162d00012e0007000007002e8436a55b162d0001220008000007002e8536a55b162df8002e0007000007002e8636a55b162df800220008000007002e8736a55b162d00012e0007000007002e8836a55b162d00012e0007000007002e8936a55b162d00012e0007000007002e8a36a55b162d00012e0007000007002e8b36a55b162d00012f0007000007002e8c36a55b162d00012e0007000007002e8d36a55b162d0001220008000007002e8e36a55b162d00012e0007000007002e8f36a55b162d00012e0007000007002e9036a55b162df0002e0007000007002e9136a55b162df0002e0007000007002e","type":"crayonic.amb.l1.data.v1_0_0"}],"idData":{"accessLevel":0,"assetId":"0xe79c12558091f6c29ce10308da0d9acee32992b4f48357c0a00bc03cf41cf3aa","createdBy":"0x2Ad4A05483314D63dbF6027e11FDf724a57171a5","dataHash":"0x728c98950f152517e759f88e6f74614fa9f19a4201a79e8eed6569a688ebcfb2","timestamp":1537554114},"signature":"0xba0a6fd32d39c2b67415a9b8ad5566b7f36f2150cb69f23a24f80eac84fa3e7909eb482f295bde02e0b28b836cc41c66a7565736c9d318820325c8d303dd689c1b"},"metadata":{"bundleId":null,"entityUploadTimestamp":1537554123}}
=========================

--------------------------------
     EVENT Accepted
--------------------------------

>>> Server to device : Bytes count = 39

ff d2 01 03 00 00 20 54 98 4e 41 1a 0a c1 17 1f ef 2d 6b ab e9 8f 94 c9 52 bc 07 8e 64 65 1b df 6f 0d 27 0a 0f 44 7d

<<< device to server
90 ff

```

-------


## Usage of server

* make release -- make release. After this you can start
* make start -- start as daemon
* make stop -- stop daemon
* make run -- start in console
* make test -- start functional tests

All config values for release keeps in sys.config file in root.

When you starts from console, it will keeps values from src/apdu_proxy.app.src file.

The current port is 5555

### Requirements

* Erlang 19 or higher
* git
* make
