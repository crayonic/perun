from flask import Flask, request, abort
from requests import post, get
import json

app = Flask(__name__)

ROOT_ENDPOINT = 'https://gateway-test.ambrosus.com'
ALARMS_ENPOINT = 'https://demo-dashboard-api.ambrosus-test.com/sensor/alert'

@app.route('/<path:path>', methods=['POST'])
def serve(path):
    method = request.method
    json_body = request.json
    request_headers = request.headers
    print(ROOT_ENDPOINT+'/'+path)
    print(request_headers)
    root_response = post(ROOT_ENDPOINT+'/'+path, json=json_body)
    print(root_response)
    if not root_response.ok:
        return root_response.json(), root_response.status_code
    if any(
        data.get('type').startswith('crayonic.amb.event.alarm') and len(data.get('alarms')) > 0
        for data in root_response.json().get('content', {}).get('data', [])
    ):
        alarm_response = post(ALARMS_ENPOINT, json=json_body)
        print('alarm response', alarm_response, alarm_response.content)
    return root_response.json()

@app.route('/<path:path>', methods=['GET'])
def serve_get(path):
    method = request.method
    json_body = request.json
    request_headers = request.headers
    print(ROOT_ENDPOINT+'/'+path)
    print(request_headers)
    root_response = get(ROOT_ENDPOINT+'/'+path, json=json_body)
    print(root_response)
    return root_response.json(), root_response.status_code

if __name__ == '__main__':
    app.run(host='localhost', port=80, debug=False, threaded=True)
