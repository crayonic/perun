all: compile

.PHONY: compile
compile:
	./rebar3 compile

.PHONY: clean
	rm -rf log/*

.PHONY: run
run: clean
	./rebar3 shell

.PHONY: start
start:
	_build/default/rel/apdu_proxy/bin/apdu_proxy start

.PHONY: console
console:
	_build/default/rel/apdu_proxy/bin/apdu_proxy remote_console

.PHONY: stop
stop:
	_build/default/rel/apdu_proxy/bin/apdu_proxy stop

.PHONY:
release:
	./rebar3 release
	rm -rf _build/default/rel/apdu_proxy/log/*


.PHONY: deploy
deploy: release
	rsync -arltxSRzv \
	    --exclude _build \
	    --exclude log \
	    . server:~/apdu_proxy

.PHONY: test
test:
	rm -rf test/ct_logs
	./rebar3 eunit --application=apdu_proxy
	./rebar3 ct

.PHONY: docker
docker:
	docker build .
