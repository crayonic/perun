# Security protocols overview

There are multiple reasons to secure communication among devices. Authentication for administative use, encryption for information exchange or authentication in a network of devices.

####Supported by L1, L1M and L2: 

1. Secure chat - Signal Protocol
1. Authentication of admin commands
1. Network affiliation - Device group authentication


---------------------------

## Secure chat - Signal Protocol
Two devices can exchange messages over encrypted channel based on Signal Protocol if following preconditions are met. 

#### Preconditions : 

1. Device support 2-way APDU communication
1. Sevice supports [Signal Protocol](https://signal.org/docs/)  as in [Signal Protocol C](https://github.com/signalapp/libsignal-protocol-c)  or as in [Signal Protocol JAVA](https://github.com/signalapp/libsignal-protocol-java) 
1. Device is capable of establishing Signal Session by exchanging Signal Init Messages.

#### Legend :
**Pre Key Bundle** - Key pairs, Identity and other data needed to setup signal protocl encryption at any point in time
**Public Pre Key Bundle** - Public keys, public identities and public data needed to setup Signal session
**Signal session** - represents states and keys used for encrypted communication among two paties over Signal Protocol
`APDU INIT SIGNAL` - APDU Write Record command used for distribution of  Public Pre Key Bundle
`APDU ENC SIGNAL` - APDU Write Record command used as carrier for encrypted paylod over Signal session

------------

### Encrypted message exchange 
Two devices can exchange APDU commands over signal protocol by encapsulation of such command as encrypted payload into `APDU ENC SIGNAL` header. Succesful decrytion by other device is confirmed by SW `OK` or `OK-WAIT`. Any other SW is considered a failure. P1 is set to C5 for every APDU record representing Cipher 5ignal.


#### APDU command ENC SIGNAL message
Carries encrypted APDU command for further execution. This is basic type of message exchanged among peers over encrypted signal channel. Encrypted payload represents either PreKey Signal message or Signal message, where data contain encrypted apdu command.

| CLA | INS | P1 | P2 | LE 00 | LE 01 - 02 | Data |
| --- | --- | --- | --- | --- | --- | --- |
| `FF` | `D2` | `C5` | `00` | `00` | `Data length` | `Encrypted payload` |

#### Status words : 
| Name | Bytes | Description | 
| --- | --- | --- | 
| `OK` | `90 00` |  decrypted correctly and encapsulated APDU command was successfuly executed 
| `OK-WAIT` | `90 FF` | same as `OK` + other device would like to send response APDU command | 
| `DEC-ERR` |  `TBD` | decryption failed | 
| `Any other` |  `XX XX` | decrypted correctly, response code of encapsulated APDU command | 

### Encrypted messages exchange
Let's assume use case where Alice wants to exchange messages securely between alice and Bob, triggering some apdu command on the other side. Response of Bob will be even another encrypted apdu command which will be triggered on Alice`s side. 

![ ](./img/diagram_signal_loop.jpg  "Encrypted mesage overview")

1. Alice wants to send APDU X to Bob over encrypted channel. 
1. Alice creates APDU X and encrypts it with the original apdu payload.
1. Alice sends the encrypted Signal Message.
1. Bob receives the message and decrypts it extracting APDU X
1. Bob triggeres the command of APDU X.
1. Bob responds with `OK-Wait`
1. Bob responds with encapsuated and encrypted APDU Y.
1. Alice decrypts the received signal message and extracts APDU Y.
1. Alice triggeres the APDU Y.
1. Alice responds with `OK`.

#### More on Signal Enc and APDU payload 
**Abstraction**
Signal protocol provides encrypted layer for message exchange, which enables us to abstract to higher level. If we setup `Signal Session` correctly on both peers (Initial message exchange), it is possible to easily encrypt(..) / decrypt(..) messages with this `Signal Session` object. For more info check test of Signal Protocol [test_session_builder.c](https://github.com/signalapp/libsignal-protocol-c/blob/master/tests/test_session_builder.c) or [SessionBuilderTest.java](https://github.com/signalapp/libsignal-protocol-java/blob/master/tests/src/test/java/org/whispersystems/libsignal/SessionBuilderTest.java)

**Encryption and Auto correcting Signal**
By default Signal Session encrypt function creates SignalMessage. If there was an interruption of encrypted channel for some reason (fresh signal session, skipped messages, failed to decrypt) Signal Session will automatically create PreKeySignalMessage, which is longer in bytes. It contains all the keys necessary to inform other side of the encryption reset and provide signal ratchet key exchange.

**Deserialization**
Signal Protocol does not implement inteligent management of the encrypted channel. Because of this you have to implement own management for incomming bytes and their deserialization. 
In Java implementation there are basically two message types PreKeySignalMessage and SignalMessage both of which can be deserialized using signal protobuf structures. Signal Protocol does not identify the message type for you.


**Encapsulated APDU**
Payload inside of the encrypted data tranferred by Signal should be an APDU Command. This ensures full compatibility with unencrypted commands because the same APDU headers and payloads are encapsuated inside of Signal messages. 

**2-Way APDU**
2 way apdu protocol is used to exchange encrypted apdu messages. Client (side sending the 1st message) starts to communicate with server and server can respond with ENC APDU. This message ping pong can be stopped by Client sending SW `OK` or any ERROR SW.

------------

### Initial message exchange
Let's assume simple use case where Alice wants to establish encrypted channel with Bob over Signal Protocol. By definition of sigal protocol, Pre Key Bundle has to be exchanged among any two peers to establish Signal session on both sides.


#### APDU command Public INIT SIGNAL message
Carries encrypted PreKeys and init values.

| CLA | INS | P1 | P2 | LE 00 | LE 01 - 02 | Data |
| --- | --- | --- | --- | --- | --- | --- |
| `FF` | `D2` | `C5` | `11` | `00` | `Data length` | `Init Signal Protocol payload` |

![ ](./img/diagram_signal_init.jpg  "Initial message exchange")

1. Alice creates **Alice's Signal session** with Alice's Pre Keys Bundle only.
1. Alice sends **Alice's Public Pre Keys Bundle** in `APDU INIT SIGNAL` to Bob.
1. Alice awaits `OK-WAIT`. Any other SW means failure.
1. Bob creates **Bob's Signal session** using received **Alice's Public Pre Key Bundle** and **Bob's Pre Key Bundle**. 
1. From now on Bob can send encrypted messages to Alice.
1. Bob responds with `OK-WAIT`.
1. Bob encrypts a NONCE which genearates Pre Keys Bundle Message using established Signal session.
1. Bob encapsulates Pre Key Bundle Message (from previous step) into `APDU ENC SIGNAL` and sends it to Alice.
1. Bob awaits `OK`. Any other SW means failure.
1. Alice decrypts `APDU ENC SIGNAL`from Bob using local keys in previously created Signal session. Successfuly decrypted data are then executed as APDU command by Alice.
1. Alice adds **Bob's Public Pre Keys** to **Alice's Signal session**. 
1. From now on Alice can send encrypted Signal messages to Bob.
1. Alice responds with `OK`.

#### Init Signal Protocol payload
To establish connection among different APIs of signal protocol we present custom message to exchange only necessary information of the PreKey bundle

```Java
// from PreKeyBundle.java
public class PreKeyBundle {

  private int         registrationId;  // any number - not used in our case - lets set it to 0

// given by other side in APDU Init Signal data
  uint32_t        deviceId; 

  uint32_t    preKeyId; 
  private ECPublicKey preKeyPublic;  // KEY_SIZE = 33 according to ECPublicKey.java

  uint32_t       signedPreKeyId;
  private ECPublicKey signedPreKeyPublic; // KEY_SIZE = 33 according to ECPublicKey.java
  private byte[]      signedPreKeySignature;

  private IdentityKey identityKey;

  ...
}
```

```C
// from session_pre_key.c
struct session_pre_key_bundle {

    signal_type_base base;
    uint32_t registration_id;  // any number - not used in our case - lets set it to 0
    int device_id;
    uint32_t pre_key_id;
    ec_public_key *pre_key_public;
    uint32_t signed_pre_key_id;
    ec_public_key *signed_pre_key_public;
    signal_buffer *signed_pre_key_signature;
    ec_public_key *identity_key;

}
```

**More about  ec_public_key / ECPublicKey :** 

| key scheme TYPE   |  public key   |
| --- | --- |
| DJB_TYPE = 0x05  | 32 bytes in DJB_TYPE | 


**Total size of the payload**

|  | Registration ID | Signal Address Part |  PreKey Bundle Part | In Total |
| --- | --- | --- | --- | --- |
| Init packet size for L1 / L2 : | 4B | 8B fixed + 17B BLE mac address  |  estimated 175B fixed | 204B |
| Init packet size for Any device : | 4B | 8B fixed + variable unique device name  |  estimated 175B fixed + variable bytes | 187B fixed + variable part for name | 


**Data**

| Registration ID | Device ID |  User Name Length | User Name  |  OneTime PreKey ID |  OneTime PreKey | OneTime Signed PreKey ID |  OneTime Signed PreKey  |  OneTime Signed PreKey Signature Length | OneTime Signed PreKey Signature Bytes | Identity PublicKey | 
| --------------- | --------- | ----------------- | ---------- | ------------------ | --------------- | ------------------------ | ----------------------- | ------------------------------ | ------------------------------------- | ------------------ |
| uint32_t registration_id |  int32_t device_id  |  size_t name_len | const char  name  |  uint32_t  preKeyId |  ec_public_key preKeyPublic  |  uint32_t  signedPreKeyId |  ec_public_key signedPreKeyPublic  |  size_t length of signal_buffer signedPreKeySignature   |  bytes of signal_buffer signedPreKeySignature   |  ec_public_key  identityKey |  
| 4B | 4B | 4B | size by previous  | 4B |  33 B | 4B |  33 B | 4B | 64 B | 33 B |  



---------------------------
## Authentication of admin commands
To prevent unauthorized party from executing administration APDU commands on device, simple authentication was introduced. 

1. Device was given preshared  authentication ADM_Key during device provisioning. 
1. Any party sending administration admin command has to provide **Data value** followed with 8 byte random **Nonce** and **Signature** S = Sig(SHA256(data | nonce)). Such concatenated byte array represents APDU Record data part.


#### Any admin APDU command
Carries encrypted APDU command for further execution. 

| CLA | INS | P1 | P2 | LE 00 | LE 01 - 02 | Data |
| --- | --- | --- | --- | --- | --- | --- |
| `FF` | `D2` | `XX` | `XX` | `00` | `Data length` | `Data` `Random nonce` `Signed Nonce` |

---------------------------

## Network affiliation - Device group authentication
It is a mechanism to verify that device is part of a group of devices, otherwise network. P1 is C6 for every Cipher 6roup APDU record.

1. Device is given preshared **NW_Key** during device provisioning.
1. To verify if such device is part of a group: 
    1. Another party requests challenge/response authentication with such **NW_Key** they both share.
    1. Another party sends APDU message encrypted with NW_Key and expects SW response **OK** or **OK-WAIT**. Specific header 
   
#### APDU command ENC NW - APDU
Carries encrypted APDU command for further execution. 

| CLA | INS | P1 | P2 | LE 00 | LE 01 - 02 | Data |
| --- | --- | --- | --- | --- | --- | --- |
| `FF` | `D2` | `C6` | `00` | `00` | `Data length` | `Encrypted APDU command` |

#### APDU command ENC NW - Reponse
Carries encrypted APDU command for durther execution. 

| CLA | INS | P1 | P2 | LE 00 | LE 01 - 02 | Data |
| --- | --- | --- | --- | --- | --- | --- |
| `FF` | `D2` | `C6` | `FF` | `00` | `Data length` | `TBD`  `Encrypted response to challenge` |



#### Status words : 
| Name | Bytes | Description | 
| --- | --- | --- | 
| `OK` | `90 00` |  decrypted correctly and encapsulated APDU command was successfuly executed 
| `OK-WAIT` | `90 FF` | same as `OK` + other device would like to send response APDU command | 
| `DEC-ERR` |  `TBD` | decryption failed | 
| `Any other` |  `XX XX` | decrypted correctly, response code of encapsulated APDU command | 