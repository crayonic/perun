# APDU TCP-HTTP proxy
#
# VERSION               0.0.1

FROM      ubuntu
MAINTAINER Sergey Loguntsov <loguntsov@gmail.com>

RUN apt-get update && yes | apt-get install make git wget gnupg libgcrypt20-dev gcc-multilib g++-multilib libglib2.0-dev
RUN wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb && dpkg -i erlang-solutions_1.0_all.deb
RUN apt-get update
# RUN apt-cache show esl-erlang | grep Ver
RUN yes | apt-get install esl-erlang=1:21.1.3
# Dependencu for libsibnal-c tests
RUN yes | apt-get install libssl-dev libcrypto++-dev
#RUN yes | apt-get install build-essential
RUN yes | apt-get install cmake
RUN yes | apt-get install libc6

RUN yes | apt-get install python3 python3-pip
#RUN pip3 install virtualenv
COPY ./veles ~/veles 
#WORKDIR ~/veles
#RUN python3 -m virtualenv venv
#RUN . ./venv/bin/activate
RUN pip3 install requests flask
#CMD ["python3", "web.py"]
#EXPOSE 5000

COPY . ~/apdu_proxy
WORKDIR ~/apdu_proxy
RUN make release
#CMD [ "make", "run" ]
EXPOSE 5555

CMD ["sh", "services.sh"]
