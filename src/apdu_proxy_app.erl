-module(apdu_proxy_app).
-author("begemot").

-behaviour(application).

%% Application callbacks
-export([
    start/2, stop/1,
    get_env/1,
    signal_server_address/0
]).

-define(APP, apdu_proxy).

start(_StartType, _StartArgs) ->

    apdu_proxy_config_static:init(),

    { ok, Pid } = apdu_proxy_sup:start_link(),
    {ok, _} = ranch:start_listener(apdu_listener, ranch_tcp, [
        {port, get_env(tcp_port) }
    ], apdu_protocol, []),

    { ok, Pid }.

stop(_State) ->
    ok.

get_env(Key) ->
    { ok, Value } = application:get_env(?APP, Key),
    Value.

signal_server_address() ->
    get_env(signal_server_address).
        

%%%===================================================================
%%% Internal functions
%%%===================================================================
