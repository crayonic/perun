-module(apdu_protocol).
-author("Sergey Loguntsov <loguntsov@gmail.com>").

-behaviour(ranch_protocol).
-export([
    start_link/4
]).

-behaviour(gen_server).
-export([
    init/1, terminate/2,
    handle_call/3,
    handle_cast/2,
    handle_info/2
]).

% unit tests
-export([
    decode_payload_test/0
]).

-define(TIMEOUT, 5000).

-record(encription, {
    type :: atom(),
    session :: erl_signal_nif:session(),
    client_address :: erl_signal_nif:address()
}).

-record(state, {
    state :: atom(),
    ref :: reference(),
    transport :: atom(),
    socket :: port(),
    opts :: #{},
    assets :: binary(),
    buffer = <<>> :: binary(),
    encription = undefined :: undefined | #encription{}
}).

-include_lib("erl_signal/include/erl_signal.hrl").

%% API.

start_link(Ref, Socket, ranch_tcp = Transport, Opts) ->
    gen_server:start_link(?MODULE, [ Ref, Socket, Transport, Opts ], []).


init([ Ref, Socket, Transport, Opts ]) ->
    lager:info(" ~p Connected ~p", [ Socket, Ref ]),
    State = #state{
        ref = Ref,
        transport = Transport,
        socket = Socket,
        opts = Opts
    },
    ok = Transport:setopts(Socket, [
        {active, once},
        {packet, raw}
    ]),
    { ok, State }.

handle_cast(terminate, State) ->
    { stop, normal, State };

handle_cast(_, State) ->
    { noreply, State }.

handle_call(_, _From, State) ->
    { reply, error, State }.

handle_info({ tcp_error, _Socket, Reason}, State) ->
    { stop, Reason, State };

handle_info({ tcp_closed, _Socket }, State) ->
    { stop, normal, State };

handle_info({ tcp, _Socket, Data }, State) ->
    lager:info("~p Received ~p bytes. ~s", [ State#state.socket, size(Data), hex_util:bin_to_hex(Data) ]),
    NewState = add_buffer(State, Data),
    #state{
        transport = Transport,
        socket = Socket
    } = State,
    case Transport:setopts(Socket, [
        {active, once}
    ]) of
        ok ->
            { noreply, NewState };
        {error,einval} ->
            { stop, einval, NewState }
    end;

handle_info(_, State) ->
    { noreply, State }.

terminate(_Reason, State) ->
    socket_close(State),
	ok.

%% Internal.

socket_close(State) ->
    #state{
        transport = Transport,
        socket = Socket
    } = State,
	catch Transport:close(Socket),
	gen_server:cast(self(), terminate),
	lager:info("~p Socket is closed", [ State#state.socket ]),
	State#state{
	    socket = undefined,
	    transport = undefined
	}.

send(State, { event, Data }) ->
    Length = size(Data),
    send(State, <<16#FF, 16#D2, 16#F1, 16#00, 16#00, Length:16/big-unsigned-integer, Data/binary>>);

send(State, { enc_event, Data }) ->
    Length = size(Data),
    send(State, <<16#FF, 16#D2, 16#C5, 16#00, 16#00, Length:16/big-unsigned-integer, Data/binary>>);


send(State, ready_to_accept_event) ->
    send(State, <<16#FF, 16#D2, 16#01, 16#00, 16#00, 16#00, 16#00>> );

send(State, { event_accepted, Data }) ->
    Hash = crypto:hash(sha256, Data),
    send(State, <<16#FF, 16#D2, 16#01, 16#01, 16#00, 16#00, 16#20, Hash/binary>>);

send(State, ok) ->
    send(State, <<16#90, 16#00>>);

send(State, error) ->
    send(State, <<16#80, 16#01>>);

send(State, 'ok-wait') ->
    send(State, <<16#90, 16#FF>>);


send(State = #state{ transport = undefined }, Binary ) ->
    lager:error("Cant send for closed connection: ~p", [ Binary ]),
    State;

send(State, Binary) when is_binary(Binary)->
    #state{
        transport = Transport,
        socket = Socket
    } = State,
    lager:info("~p Send ~s", [ State#state.socket, hex_util:bin_to_hex(Binary) ]),
    ok = Transport:send(Socket, Binary).


add_buffer(State, Binary) ->
    #state{
        buffer = Buffer
    } = State,
    NewBuffer = <<Buffer/binary, Binary/binary>>,
    NewState0 = State#state{
        buffer = NewBuffer
    },
    case handle(NewBuffer, NewState0) of
        { rest, Rest, NewSt } ->
            NewSt0 = NewSt#state{
                buffer = Rest
            },
            add_buffer(NewSt0, <<>>);
        { undefined, NewSt } ->
            NewSt
    end.

set_state(State, Atom) ->
    State#state{
        state = Atom
    }.

%% Hello packet (authentication)

handle(<<"ping", _/binary>>, State) ->
    send(State, <<"pong">>),
    { rest, <<>>, State };
handle(<<16#FF, 16#D2, 16#20, 16#00, 16#00, 16#00, 16#48, _Identity:40/binary, Assets:32/binary, Rest/binary>>, State = #state{ assets = undefined }) ->
    send(State, 'ok-wait'),
    send(State, ready_to_accept_event),
    NewState = State#state{
        assets = list_to_binary(string:to_lower(binary_to_list(hex_util:bin_to_hex(Assets))))
    },
    { rest, Rest, NewState };

handle(<<16#FF, 16#D2, 16#C5, 16#11, 16#00, Len:16/unsigned-integer, Handshake:Len/binary, Rest/binary>> = _Binary, State = #state{ assets = undefined }) ->
    { ok, EncSession } = erl_signal:new(),
    ok = erl_signal:generate_identity_keys(EncSession),
    Address = apdu_proxy_app:signal_server_address(),
    case erl_signal:handshake_accept(EncSession, Address, Handshake) of
        { ok, ClientAddress, _ServerHandshake } ->
            <<_Identity:40/binary, Assets:32/binary>> = ClientAddress#es_address.name,
            { ok, EH } = erl_signal_nif:encode(EncSession, ClientAddress, <<16#FF, 16#D2, 16#01, 16#00, 16#00, 16#00, 16#00>>), % ready_to_accept_event
            lager:info("Crypted connection from ~p ~p", [ ClientAddress, Handshake ]),
            send(State, 'ok-wait'),
            send(State, { enc_event, EH}),
            NewState = State#state{
                encription = #encription{
                    type = erl_signal,
                    session = EncSession,
                    client_address = ClientAddress
                },
                assets = list_to_binary(string:to_lower(binary_to_list(hex_util:bin_to_hex(Assets))))
            },
            { rest, Rest, NewState };
        { error, _ } ->
            send(State, error),
            socket_close(State),
            { undefined, State }
    end;

%% Event packet
handle(<<16#FF, 16#D2, _RecordIdentifier:8/unsigned-integer, _SubRecord:8/unsigned-integer, 0, Length:16/big-unsigned-integer, Data:Length/binary, Rest/binary>>, State) ->
    lager:info("Data ~s ~n ~p", [ Data, Data ]),
    Result = case State#state.encription of
        undefined -> { ok, Data };
        #encription{ type = erl_signal, client_address = ClientAddress, session = EncSession } ->
            case erl_signal_nif:decode(EncSession, ClientAddress, Data) of
                { ok, EncriptedData } ->
                    lager:info("Encripted data: ~p", [ EncriptedData ]),
                    { ok, EncriptedData };
                { error, _ } = Error -> Error
            end
    end,
    case Result of
        { ok, NewData } ->
            try
                case decode_payload(NewData) of
                    { ok, NewData0 } ->
                        Log = iolist_to_binary(["I got this event: ",10, io_lib:format("~s", [ NewData ]), 10, 10, hex_util:bin_to_hex(NewData), 10, 10,
                            "Converted to: ", 10, io_lib:format("~s", [ NewData0 ]), 10, 10, hex_util:bin_to_hex(NewData0), 10, 10]),
                        { _, Time1, Time2} = erlang:now(),
                        ok = file:write_file("./log/d" ++ integer_to_list(Time1 * 1000000 + Time2)++ ".log", Log),
                        case http_request(State, NewData0) of
                            ok -> send(State, ok);
                            fail -> send(State, error)
                        end;
                    { error, Reason } ->
                        lager:error("Can't recognize payload by reason: ~p ~p", [ Reason, NewData ]),
                        send(State, error)
                end
            catch
                E:R ->
                    send(State, error),
                    erlang:raise(E, R, erlang:get_stacktrace())
            end,
            { rest, Rest, State };
        { error, _Reason } ->
            send(State, error),
            { rest, Rest, State }
    end;

handle(<<16#FF, 16#D2, _RecordIdentifier:8/unsigned-integer, _SubRecord:8/unsigned-integer, N:1/binary, Rest/binary>>, State) when N =/= <<0>> ->
    NewState = socket_close(State),
    { rest, Rest, NewState };

handle(<<16#FF, Byte:1/binary, Rest/binary>> = Packet, State) when Byte =/= <<16#D2>> ->
    lager:info("Bad packet ~p", [ Packet ]),
    send(State, error),
    NewState = socket_close(State),
    { rest, Rest, NewState };

handle(<<16#FF, _/binary>> = _Packet, State) ->
    { undefined, State };

handle(<<16#90, 16#00, Rest/binary>>, State) ->
    NewState = socket_close(State),
    { rest, Rest, NewState };

handle(<<16#90, 16#FF, Rest/binary>>, State) ->
    lager:info("Parsed ok-wait"),
    { rest, Rest, State };
    
handle(<<_:1/binary, Rest/binary>>, State) ->
    { rest, Rest, State };

handle(_, State) ->
    { undefined, State }.

http_request(State, Data) ->
    lager:info("State ~p", [ State ]),
    Url = <<(apdu_proxy_app:get_env(url))/binary, "/assets/0x", (State#state.assets)/binary, "/events">>,
    lager:info("Http request: ~s ~s ", [ Url, Data ]),
    Response = httpc:request(post, { binary_to_list(Url), [],  "application/json", binary_to_list(Data) }, [], [] ),
    lager:info("Response ~p", [ Response ]),
    case Response of
        { ok, {{_, 401, _}, _Headers, "{\"reason\":\"Authentication failed" ++ _}} ->
            lager:info("Checking of signature at end of the string ~s", [ Data ]),
            Size = size(Data) - 4,
            case Data of
                <<Rest:Size/binary, "b\"}}">> ->
                    http_request(State, <<Rest/binary, "c\"}}">>);
                _ -> fail
            end;
        { ok, {{_ ,Status, _}, _Headers, _Body}} when Status >= 200 andalso Status < 300 -> ok;
        { ok, {{_ ,_Status, _}, _Headers, _Reason}} -> fail
    end.

decode_payload(<<"{", _/binary>> = Json) -> {ok, Json };
decode_payload(<<D:1/binary, _/binary>> = HexString) when D >= <<"0">>, D =< <<"9">> ->
    P = apdu_proxy_config_static:re_payload(),
    case re:run(HexString, P, [{capture, all_but_first, binary}]) of
        nomatch -> { error, nomatch };
        { match, [ Bin1, Bin2, _ ]} ->
            Start = binary_to_integer(Bin1),
            L = size(Bin1) + size(Bin2) + 1,
            <<_:L/binary, Rest/binary>> = HexString,
            Length = size(Rest) - Start - binary_to_integer(Bin2),
%            Length = binary_to_integer(Bin2) - size(Bin1) - size(Bin2) - 2,
            <<StartBin:Start/binary, Binary:Length/binary, FinishBin/binary>> = Rest,
            Json = <<StartBin/binary, (hex_util:bin_to_hex(Binary))/binary, FinishBin/binary>>,
            { ok, Json }
    end;
decode_payload(_) ->
    { error, bad_format }.

%% EUNIT tests

-include_lib("eunit/include/eunit.hrl").

decode_payload_test() ->
    { ok, _ } = application:ensure_all_started(apdu_proxy),
    { ok, <<"{hello}">> } = decode_payload(<<"{hello}">>),
    ?assertEqual({ ok, <<"asd0102030405060708090a0b0c0d0e0f00ffhello">> }, decode_payload(<<"3:5asd",1,2,3,4,5,6,7,8,9,10,11,12,13,14,15, 0, 255, "hello">>)),
%%    { ok, <<"asd0102030405060708090A0B0C0D0E0F00FFhello">> } = decode_payload(<<"3:22asd",1,2,3,4,5,6,7,8,9,10,11,12,13,14,15, 0, 255, "hello">>),
%%    RealPayload = <<49,49,48,58,53,50,49,123,34,101,118,101,110,116,73,100,34,58,34,48,120,100,97,56,50,48,100,52,50,99,55,99,48,50,51,97,97,50,49,100,97,54,53,48,48,102,56,48,49,102,48,55,99,54,56,48,102,56,48,99,98,101,102,57,97,55,53,102,54,102,57,50,54,51,52,98,52,54,49,48,100,51,48,50,57,34,44,34,99,111,110,116,101,110,116,34,58,123,34,100,97,116,97,34,58,91,123,34,108,49,95,114,97,119,34,58,34,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,34,44,34,116,121,112,101,34,58,34,99,114,97,121,111,110,105,99,46,97,109,98,46,108,49,46,100,97,116,97,46,118,50,95,48,95,48,34,125,93,44,34,105,100,68,97,116,97,34,58,123,34,97,99,99,101,115,115,76,101,118,101,108,34,58,48,44,34,97,115,115,101,116,73,100,34,58,34,48,120,101,55,57,99,49,50,53,53,56,48,57,49,102,54,99,50,57,99,101,49,48,51,48,56,100,97,48,100,57,97,99,101,101,51,50,57,57,50,98,52,102,52,56,51,53,55,99,48,97,48,48,98,99,48,51,99,102,52,49,99,102,51,97,97,34,44,34,99,114,101,97,116,101,100,66,121,34,58,34,48,120,56,55,65,49,65,69,66,50,53,48,51,54,57,51,53,102,48,50,100,53,53,101,66,56,56,51,50,66,67,99,100,56,67,54,51,67,101,68,69,48,34,44,34,100,97,116,97,72,97,115,104,34,58,34,48,120,100,48,98,52,97,56,97,52,55,50,52,55,98,54,55,56,54,98,101,102,102,50,57,49,97,102,55,54,98,53,52,51,48,97,48,99,102,100,98,102,55,57,99,99,57,48,57,50,97,55,54,48,51,101,49,53,97,100,102,97,54,51,50,100,34,44,34,116,105,109,101,115,116,97,109,112,34,58,49,53,52,54,56,56,56,51,56,53,125,44,34,115,105,103,110,97,116,117,114,101,34,58,34,48,120,101,100,55,98,99,99,55,49,56,99,52,56,102,54,53,101,102,100,101,100,53,51,54,51,99,54,54,56,98,98,55,54,98,56,102,48,50,56,49,48,57,100,54,51,98,54,56,56,99,98,48,54,50,53,98,50,55,101,56,56,97,48,49,100,55,55,54,102,55,102,52,53,98,57,55,99,53,56,57,48,49,52,101,52,49,56,57,54,97,49,55,56,57,55,55,54,57,99,101,48,54,55,101,49,55,50,101,56,97,99,98,52,97,100,52,98,97,98,55,48,56,49,51,99,57,57,56,99,49,99,34,125,125>>,
%%    ok = decode_payload(RealPayload),


    { error, nomatch } = decode_payload(<<"1sasasdsa">>),
    { error, bad_format } = decode_payload(<<"asdasdad">>),

    JsonPacket0 = <<"{\"content\":{\"data\":[{\"l1_raw\":\"04F106525C120801211D322618410356BBFF1B003308010B1102010511FE0105411056080000001600010556F8FF0200EBFF010541F156FAFFFEFFEFFF010556050000000F000105328C1101060000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000\",\"type\":\"crayonic.amb.l1.data.v2_0_0\"}],\"idData\":{\"accessLevel\":0,\"assetId\":\"0xe79c12558091f6c29ce10308da0d9acee32992b4f48357c0a00bc03cf41cf3aa\",\"createdBy\":\"0x0C7401498719555B2119C01bB6af0dFA904AD69e\",\"dataHash\":\"0x48452f7ec2ad85ce10ca5446ef0550a1c0abb8e627b8939e656b884cf09cdf06\",\"timestamp\":1550488160},\"signature\":\"0x3838c676b649bfdc9902a52ddee9114bd35bb212ea1e09e63ffba2bde447afe204bef263947fca88c89203edcb890ccf1bc32c7d63edb557c808e958e67a0e9a1c\"},\"eventId\":\"0xb32788a9342635a6237f9217de47b315a5ff87dd2dda919d551dab314a4f2a6b\"}">>,
    BinPacket0 = <<51,49,58,53,51,52,123,34,99,111,110,116,101,110,116,34,58,123,34,100,97,116,
        97,34,58,91,123,34,108,49,95,114,97,119,34,58,34,4,241,6,82,92,18,8,1,33,29,
        50,38,24,65,3,86,187,255,27,0,51,8,1,11,17,2,1,5,17,254,1,5,65,16,86,8,0,0,0,
        22,0,1,5,86,248,255,2,0,235,255,1,5,65,241,86,250,255,254,255,239,255,1,5,86,
        5,0,0,0,15,0,1,5,50,140,17,1,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,34,44,34,116,121,112,101,34,58,34,99,114,97,121,111,110,105,99,46,97,
        109,98,46,108,49,46,100,97,116,97,46,118,50,95,48,95,48,34,125,93,44,34,105,
        100,68,97,116,97,34,58,123,34,97,99,99,101,115,115,76,101,118,101,108,34,58,
        48,44,34,97,115,115,101,116,73,100,34,58,34,48,120,101,55,57,99,49,50,53,53,
        56,48,57,49,102,54,99,50,57,99,101,49,48,51,48,56,100,97,48,100,57,97,99,101,
        101,51,50,57,57,50,98,52,102,52,56,51,53,55,99,48,97,48,48,98,99,48,51,99,
        102,52,49,99,102,51,97,97,34,44,34,99,114,101,97,116,101,100,66,121,34,58,34,
        48,120,48,67,55,52,48,49,52,57,56,55,49,57,53,53,53,66,50,49,49,57,67,48,49,
        98,66,54,97,102,48,100,70,65,57,48,52,65,68,54,57,101,34,44,34,100,97,116,97,
        72,97,115,104,34,58,34,48,120,52,56,52,53,50,102,55,101,99,50,97,100,56,53,
        99,101,49,48,99,97,53,52,52,54,101,102,48,53,53,48,97,49,99,48,97,98,98,56,
        101,54,50,55,98,56,57,51,57,101,54,53,54,98,56,56,52,99,102,48,57,99,100,102,
        48,54,34,44,34,116,105,109,101,115,116,97,109,112,34,58,49,53,53,48,52,56,56,
        49,54,48,125,44,34,115,105,103,110,97,116,117,114,101,34,58,34,48,120,51,56,
        51,56,99,54,55,54,98,54,52,57,98,102,100,99,57,57,48,50,97,53,50,100,100,101,
        101,57,49,49,52,98,100,51,53,98,98,50,49,50,101,97,49,101,48,57,101,54,51,
        102,102,98,97,50,98,100,101,52,52,55,97,102,101,50,48,52,98,101,102,50,54,51,
        57,52,55,102,99,97,56,56,99,56,57,50,48,51,101,100,99,98,56,57,48,99,99,102,
        49,98,99,51,50,99,55,100,54,51,101,100,98,53,53,55,99,56,48,56,101,57,53,56,
        101,54,55,97,48,101,57,97,49,99,34,125,44,34,101,118,101,110,116,73,100,34,
        58,34,48,120,98,51,50,55,56,56,97,57,51,52,50,54,51,53,97,54,50,51,55,102,57,
        50,49,55,100,101,52,55,98,51,49,53,97,53,102,102,56,55,100,100,50,100,100,97,
        57,49,57,100,53,53,49,100,97,98,51,49,52,97,52,102,50,97,54,98,34,125>>,

    { ok, JsonPacket0 } = decode_payload(BinPacket0),

    JsonPacket1 = <<"{\"content\":{\"data\":[{\"l1_raw\":\"04F106525C120801211D322618410356BBFF1B003308010B1102010511FE0105411056080000001600010556F8FF0200EBFF010541F156FAFFFEFFEFFF010556050000000F000105328C1101060000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000\",\"type\":\"crayonic.amb.l1.data.v2_0_0\"}],\"idData\":{\"accessLevel\":0,\"assetId\":\"0xe79c12558091f6c29ce10308da0d9acee32992b4f48357c0a00bc03cf41cf3aa\",\"createdBy\":\"0x0C7401498719555B2119C01bB6af0dFA904AD69e\",\"dataHash\":\"0x48452f7ec2ad85ce10ca5446ef0550a1c0abb8e627b8939e656b884cf09cdf06\",\"timestamp\":1550488161},\"signature\":\"0xaa41ec6235c8d222b7cd1e69a0d15bb695c3e54b2b98608a1005383cf96a92154859db944a7834467ce14580a5d84cfc7afd5b842de38b90986722645c8fe7f71c\"},\"eventId\":\"0x79e22be22e6cba555de9f84d27b52f1239ed3142d054a292a7bd58d94d2fc272\"}">>,
    BinPacket1 = <<51,49,58,53,51,52,123,34,99,111,110,116,101,110,116,34,58,123,34,100,97,116,
        97,34,58,91,123,34,108,49,95,114,97,119,34,58,34,4,241,6,82,92,18,8,1,33,29,
        50,38,24,65,3,86,187,255,27,0,51,8,1,11,17,2,1,5,17,254,1,5,65,16,86,8,0,0,0,
        22,0,1,5,86,248,255,2,0,235,255,1,5,65,241,86,250,255,254,255,239,255,1,5,86,
        5,0,0,0,15,0,1,5,50,140,17,1,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,34,44,34,116,121,112,101,34,58,34,99,114,97,121,111,110,105,99,46,97,
        109,98,46,108,49,46,100,97,116,97,46,118,50,95,48,95,48,34,125,93,44,34,105,
        100,68,97,116,97,34,58,123,34,97,99,99,101,115,115,76,101,118,101,108,34,58,
        48,44,34,97,115,115,101,116,73,100,34,58,34,48,120,101,55,57,99,49,50,53,53,
        56,48,57,49,102,54,99,50,57,99,101,49,48,51,48,56,100,97,48,100,57,97,99,101,
        101,51,50,57,57,50,98,52,102,52,56,51,53,55,99,48,97,48,48,98,99,48,51,99,
        102,52,49,99,102,51,97,97,34,44,34,99,114,101,97,116,101,100,66,121,34,58,34,
        48,120,48,67,55,52,48,49,52,57,56,55,49,57,53,53,53,66,50,49,49,57,67,48,49,
        98,66,54,97,102,48,100,70,65,57,48,52,65,68,54,57,101,34,44,34,100,97,116,97,
        72,97,115,104,34,58,34,48,120,52,56,52,53,50,102,55,101,99,50,97,100,56,53,
        99,101,49,48,99,97,53,52,52,54,101,102,48,53,53,48,97,49,99,48,97,98,98,56,
        101,54,50,55,98,56,57,51,57,101,54,53,54,98,56,56,52,99,102,48,57,99,100,102,
        48,54,34,44,34,116,105,109,101,115,116,97,109,112,34,58,49,53,53,48,52,56,56,
        49,54,49,125,44,34,115,105,103,110,97,116,117,114,101,34,58,34,48,120,97,97,
        52,49,101,99,54,50,51,53,99,56,100,50,50,50,98,55,99,100,49,101,54,57,97,48,
        100,49,53,98,98,54,57,53,99,51,101,53,52,98,50,98,57,56,54,48,56,97,49,48,48,
        53,51,56,51,99,102,57,54,97,57,50,49,53,52,56,53,57,100,98,57,52,52,97,55,56,
        51,52,52,54,55,99,101,49,52,53,56,48,97,53,100,56,52,99,102,99,55,97,102,100,
        53,98,56,52,50,100,101,51,56,98,57,48,57,56,54,55,50,50,54,52,53,99,56,102,
        101,55,102,55,49,99,34,125,44,34,101,118,101,110,116,73,100,34,58,34,48,120,
        55,57,101,50,50,98,101,50,50,101,54,99,98,97,53,53,53,100,101,57,102,56,52,
        100,50,55,98,53,50,102,49,50,51,57,101,100,51,49,52,50,100,48,53,52,97,50,57,
        50,97,55,98,100,53,56,100,57,52,100,50,102,99,50,55,50,34,125>>,

    { ok, JsonPacket1 } = decode_payload(BinPacket1),

    ok.

        

    
    




