-module(apdu_proxy_config_static).

%% API
-export([
    init/0,
    re_payload/0
]).

-define(DB, apdu_proxy_config_static_db).

init() ->
    { ok, P } = re:compile("(*ANY)^(\\d+)\\:(\\d+)(.*)", [ dotall ]),
    beam_storage:compile(?DB, [
        { re_payload, {term , P }}
    ]),
    ok.

re_payload() ->
    ?DB:re_payload().